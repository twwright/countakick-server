/**
 * Created by Tom on 11/07/2015.
 */

var express = require('express');
var router = express.Router();

// include the sessions database model
var SessionModel = require('../models/session.js');

/** Retrieves a single session object matched by :sessId */
router.get('/:sessId', function(req, res, next) {
    SessionModel.findOne({ _id: req.params.sessId }, function(err, session) {
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err),
                jwt: req.jwtPayload
            };
            return next(error);
        }
        res.json(session || {} );
    });
});

/** Retrieves an array of sessions matched by given search parameters */
// TODO implement search parameters
router.get('/', function(req, res, next) {
    // resolve query parameters from request
    var deviceId = req.query.deviceId || null;
    var findParams = (deviceId != null) ? { deviceId: deviceId } : {};
    var sortBy = req.query.sortBy || null;
    var sortOrder = req.query.sortOrder || null;
    var sortParams = {};
    if(sortBy != null)
        sortParams[sortBy] = sortOrder;
    var offset = req.query.offset || 0;
    var limit = req.query.limit || 10;
    // perform query
    SessionModel.find(findParams).sort(sortParams).skip(offset).limit(limit).exec(function(err, sessions) {
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err),
                jwt: req.jwtPayload
            };
            return next(error);
        }
        res.json(sessions);
    });
    // retrieve sessions from database
    /*
    SessionModel.find(function(err, sessions) {
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err),
                jwt: req.jwtPayload
            };
            return next(error);
        }
        res.json(sessions);
    });
    */
});

/** Inserts a new session included as request body */
router.post('/', function(req, res, next) {
    // create session from request body
    var session = new SessionModel(req.body);

    // attempt to create new user object (validation applied)
    session.save(function(err, session) {
        if(err)
        {
            var error = {};
            if(err.name === 'ValidationError')
                error = {
                    code: 400,
                    error: 'bad-data',
                    message: 'Submitted data or command failed internal validation! Ensure submitted data is correct according to the API constraints',
                    debug: err.errors,
                    jwt: req.jwtPayload
                };
            else
                error = {
                    code: 500,
                    error: 'db',
                    message: 'Internal database error. Please contact administrator.',
                    debug: JSON.stringify(err),
                    jwt: req.jwtPayload
                };
            return next(error);
        }
        else
        {
            res.json(session);
        }
    });
});

/** Deletes a single session object matches by :sessId */
router.delete('/:sessId', function(req, res, next) {
    SessionModel.findOneAndRemove({ _id: req.params.sessId }, function(err, session) {
        // respond appropriately if error occurred
        if(err)
        {
            var error = {
                code: 500,
                error: 'db',
                message: 'Internal database error. Please contact administrator.',
                debug: JSON.stringify(err),
                jwt: req.jwtPayload
            };
            return next(error);
        }
        // return removed user document or an empty object
        res.json(session || {});
    });
});

module.exports = router;
