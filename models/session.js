/**
 * Created by Tom on 11/07/2015.
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// define the schema for each event
var EventSchema = new Schema({
    timestamp: { type: Date, required: true },
    event: { type: Number, required: true }
}, {_id: false});

// define the session schema
var SessionSchema = new Schema({
    deviceId: { type: String, required: true },
    start: { type: Date, required: true },
    end: { type: Date, required: true},
    events: { type: [EventSchema], default: [] },
    avgHeartRate: { type: Number }
}, { collection: 'sessions'});

/*SessionSchema.path('events').validate(function(events, done) {
    done(events.length > 0);
}, 'Session must contain events');*/

module.exports = mongoose.model('session', SessionSchema);