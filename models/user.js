/**
 * Created by Tom on 11/07/2015.
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

// define the user schema
var UserSchema = new Schema({
    username: { type: String, required: true },
    password: { type: String, required: true },
    salt: { type: String, required: true },
    deviceIds: { type: [String], required: true }
}, { collection: 'users'});

// unique username validation
UserSchema.path('username').validate(function(value, done) {
    console.log("Validating username...");
    this.constructor.findOne({
        _id: { $ne: this._id},
        username: this.username
    }, function (err, results) {
        if (err)
            throw err;
        done(!results);
    });
}, 'Username must be unique, username already exists');

// unique device IDs validation
UserSchema.path('deviceIds').validate(function(value, done) {
    console.log("Validating device IDs...");
    this.constructor.findOne({
        _id: { $ne: this._id },
        deviceIds: { $in: this.deviceIds }
    }, function(err, results) {
        if (err)
            throw err;
        done(!results);
    });
}, 'Device IDs must be unique, one or more device IDs already exists');

module.exports = mongoose.model('user', UserSchema);