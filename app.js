var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var config = require('./lib/config');
var routes = require('./routes/index');
var users = require('./routes/users');
var sessions = require('./routes/sessions');

var userModel = require('./models/user');

var app = express();


console.log("Starting M2M Countakick Application Server...");
// connect database
console.log("Connecting to MongoDB database...");
mongoose.connect('mongodb://' + config.db.user + ':' + config.db.password + '@' + config.db.host + '/' + config.db.database);
var db = mongoose.connection;
db.on('error', function(err) {
  console.error("Database connection error: " + err);
});
db.on('disconnected', function() {
  console.log("Database disconnected! Reconnecting...");
  mongoose.connect('mongodb://' + config.db.user + ':' + config.db.password + '@' + config.db.host + '/' + config.db.database);
});
db.on('open', function( callback) {
  console.log("Database connection established!");
});

// define app port
app.set('port', config.port);
console.log("Listening on port " + config.port + "...");
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

console.log("Configuring routes...");

app.use('/', routes);
app.post('/api/auth', function(req, res, next) {
  if(!(req.body.username && req.body.password))
  {
    var error = {
      code: 400,
      error: "bad-data",
      message: 'Missing username and/or password'
    };
    return next(error); // invoke error handler directly otherwise we pass to API authentication handler!
  }
  // query for user record
  userModel.findOne({ username: req.body.username }, function(err, user) {
    if(err)
    {
      var error = {
        code: 500,
        error: "db",
        message: 'Internal database error occurred. Please contact administrator.',
        debug: JSON.stringify(err)
      };
      return next(error); // invoke error handler directly otherwise we pass to API authentication handler!
    }
    var authenticated = false;
    if(user)
    {
      // calculate password hash
      var clientHash = bcrypt.hashSync(req.body.password, user.salt);
      if(clientHash === user.password)
        authenticated = true;
    }

    if(authenticated)
    {
      // create token
      var token = jwt.sign(user, config.jwt.secret, { expiresInMinutes: config.jwt.expiry });
      // return user and token data to user
      res.json({ user: user, token: token });
    }
    else
    {
      var error = {
        code: 401,
        error: "auth",
        message: 'Bad username/password. Please authenticate with valid credentials.',
        debug: (user) ? "Bad username" : "Bad password"
      };
      return next(error); // invoke error handler directly otherwise we pass to API authentication handler!
    }
  });
});

/** Inserts the new user object included in request body
 *  Copied from user endpoints - non-authentication endpoint to allow user creation
 *  */
app.post('/api/createUser', function(req, res, next) {
  // create password salt and generate hash
  if(req.body.password)
  {
    req.body.salt = bcrypt.genSaltSync();
    req.body.password = bcrypt.hashSync(req.body.password, req.body.salt);
  }

  // create user from request body
  var user = new userModel(req.body);

  // attempt to create new user object (validation applied)
  user.save(function(err, user) {
    if(err)
    {
      var error = {};
      if(err.name === 'ValidationError')
        error = {
          code: 400,
          error: 'bad-data',
          message: 'Submitted data or command failed internal validation! Ensure submitted data is correct according to the API constraints',
          debug: err.errors,
          jwt: req.jwtPayload
        };
      else
        error = {
          code: 500,
          error: 'db',
          message: 'Internal database error. Please contact administrator.',
          debug: JSON.stringify(err),
          jwt: req.jwtPayload
        };
      return next(error);
    }
    else
    {
      res.json(user);
    }
  });
});

// API authentication handler
app.use('/api/*', function(req, res, next) {
  console.log("Authenticating API request...");
  // attempt to retrieve token from query params or headers
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  // if token exists, proceed to verification
  if(token)
  {
    // attempt to decode token
    jwt.verify(token, config.jwt.secret, function(err, payload) {
      if(err)
      {
        error = {
          code: 401,
          error: 'auth',
          message: 'Invalid JSON Web Token',
          debug: err,
          jwt: "none"
        };
        next(error); // invoke error handler directly otherwise we pass to API endpoints!
      }
      // TODO check user exists here
      // save payload into request for later use
      req.jwtPayload = payload;
      next();
    });
  }
  else
  {
    var error = {
      code: 401,
      error: 'auth',
      message: 'Missing JSON Web Token',
      debug: 'Ensure a valid JWT is attached in headers (x-access-token) or query string. POST username/password to /api/auth/ to get JWT!',
      jwt: "none"
    };
    next(error); // invoke error handler directly otherwise we pass to API endpoints!
  }
});
// API endpoints
app.use('/api/user', users);
app.use('/api/session', sessions);

// catch API 404s and set error
app.use('/api/*', function(req, res, next) {
  // if no route matched at this point, set a generic 404
  var error = {
    code: 404,
    error: 'not-found',
    message: 'No resource exists here.',
    debug: 'Check your URL matches the server API!',
    jwt: req.jwtPayload
  };
  next(error);
});

// API error handler
app.use('/api/*', function (err, req, res, next) {
  // we can *hopefully* assume some error has occurred and we need to handle it
  var obj = {
    error: err.error || "Error",
    message: err.message
  };
  // return extra data under dev conditions
  if(app.get('env') === 'development') {
    obj.debug = err.debug || "No debug info available";
    obj.jwt = req.jwtPayload;
  }
  console.error('Error: ' + obj.error + ' (' + obj.message + ' - ' + obj.debug + ')');
  res.status(err.code || 400).json(obj);
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

console.log("Ready!");