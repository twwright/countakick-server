/**
 * Created by Tom on 12/07/2015.
 *
 * Contains definitions for various server configuration values
 *
 */

module.exports.port = 3000;
module.exports.db = {
    host: "localhost",
    database: "m2m",
    user: "serverUser",
    password: "password"
};
module.exports.jwt = {
    secret: "thesupersecret!",
    expiry: 60 // minutes
};